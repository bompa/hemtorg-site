'use strict';

var express = require('express');
var path = require('path');
var app = express();

app.use('/', express.static(path.join(__dirname, 'dist')))

let port = process.env.PORT || 8080;
app.listen(port, function () {
  console.log('Server listening on port ' + port);
})