import { Injectable } from '@angular/core';
import { GeoLocation } from 'ht-public/interfaces/geolocation.interface'
import { RealEstateProperties } from 'ht-public/interfaces/realestateproperties.interface'
import { RealEstateService } from '../services/realestate.service';
import { SearchResult } from '../interfaces/searchresult.interface'

@Injectable()
export class SearchService {

  constructor(private realeasteService: RealEstateService) {
  }

  public search(filter: SearchResult) {

  }
}
