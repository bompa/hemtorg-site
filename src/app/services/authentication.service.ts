import { Injectable } from '@angular/core';
import { tokenNotExpired, JwtHelper } from 'angular2-jwt';
import { ConfigService } from '../services/config.service'
import { Observable } from 'rxjs/Observable';

declare var Auth0Lock;

@Injectable()
export class AuthenticationService {
  private lockObs: Observable<any>;

  jwtHelper: JwtHelper = new JwtHelper();

  private loggedInuser: any;

  constructor(private configService: ConfigService) {
    this.lockObs = new Observable(subscriber => {
      configService.getConfig('app/services/conf/authentication.service.json')
        .subscribe(confData => {
          subscriber.next(new Auth0Lock(confData.auth0server.appid, confData.auth0server.host));
          subscriber.complete();
        });
    });

    if (this.loggedIn()) {
      this.loggedInuser = JSON.parse(localStorage.getItem('profile'));
    }
  }


  /**
   * Login.
   * 
   * @param loggedInResult called when log in operation is done
   */
  login(loggedInResult: (status: boolean) => any) {
    this.lockObs.subscribe(lock => {
      lock.show((err: string, profile: string, id_token: string) => {
        if (err) {
          throw new Error(err);
        }

        localStorage.setItem('profile', JSON.stringify(profile));
        localStorage.setItem('id_token', JSON.stringify(id_token));
        this.loggedInuser = profile;

        if (loggedInResult) {
          loggedInResult(true);
        }
      });
    });
  }

  logout() {
    localStorage.removeItem('profile');
    localStorage.removeItem('id_token');

    for (var property in this.loggedInuser) {
      if (this.loggedInuser.hasOwnProperty(property)) {
        this.loggedInuser[property] = '';
      }
    }

    this.loggedIn();
  }

  loggedIn() {
    return tokenNotExpired();
  }

  isActive(profile) {
    return this.loggedIn();
  }

  getCurrentUser() {
    return this.loggedInuser;
  }
}