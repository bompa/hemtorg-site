import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import { RealEstateProperties } from 'ht-public/interfaces/realestateproperties.interface';
import { createEmptyRealEstate } from '../model/realestate.model';
import { OptionsService } from '../services/options.service';
import { AuthenticationService } from '../services/authentication.service';
import { Query, getQueryParams } from 'ht-public/queries/query'
import { getPropertyName } from 'ht-public/names/propname'
import { IAuthConfig } from 'angular2-jwt'

@Injectable()
export class RealEstateService {

  constructor(private http: Http,
    private optionsService: OptionsService,
    private authService: AuthenticationService) {
    this.backendHost = new Observable(subscriber => {
      this.optionsService.getOptions()
        .subscribe(conf => {
          subscriber.next(conf.backendHost);
          subscriber.complete();
        });
    });
  }

  private backendHost: Observable<string>;

  sendRequest(httpReqFunc: (host, subscriber) => void) {
    return new Observable(subscriber => {
      this.backendHost.subscribe(host => {
        httpReqFunc(host, subscriber);
      })
    });
  }

  nextComplete(subscriber, value) {
    subscriber.next(value);
    subscriber.complete();
  }

  /** List estates using query, or list all if query is empty
   */
  list(query?): Observable<any[]> {
    let queryParameters = getQueryParams(query);
    return this.sendRequest((host, subscriber) => {
      this.http.get(host + '/realestates' + queryParameters, this.jwt())
        .map((response: Response) => response.json())
        .subscribe(data => this.nextComplete(subscriber, data));
    });
  }

  /** Get estate by id
   */
  get(id: string): RealEstateProperties {
    return createEmptyRealEstate();
  }


  private getBase64(file, completedImages, totalImages, subscriber) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      subscriber.next(reader.result);
      completedImages.count++;
      if (completedImages.count == totalImages) {
        subscriber.complete();
      }
    };
    reader.onerror = function (error) {
      console.error('Error getting file as base64: ', error);
    };
  }

  /** Create/register new real estate for current user
   */
  prepareAndCreate(realEstate: RealEstateProperties, host: string, subscriber) {
    return this.http.post(host + '/realestates', realEstate, this.jwt())
      .map(res => res.status)
      .catch(err => {
        subscriber.error(this.handleError(err));
        return Promise.reject(err);
      })
      .subscribe(status => {
        this.nextComplete(subscriber, status);
      });
  }

  create(realEstate: RealEstateProperties) {
    return this.sendRequest((host, subs) => {
      this.prepareAndCreate(realEstate, host, subs);
    })
  }

  public handleError(error: Response | any): string {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string = '';
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return errMsg;
  }

  private jwt() {
    // create authorization header with jwt token
    let currentUserToken = JSON.parse(localStorage.getItem('id_token'));
    let profile = JSON.parse(localStorage.getItem('profile'));
    let userID = '';
    if (profile && profile.identities) {
      if (profile.identities.length) {
        userID = profile.identities[0].user_id;
      }
    }
    if (this.authService.loggedIn() && currentUserToken) {
      let headers = new Headers(
        {
          'Authorization': 'Bearer ' + currentUserToken,
          'X-UserID': userID
        });
      return new RequestOptions({ headers: headers, withCredentials: true });
    }
  }
}