import { Injectable } from '@angular/core';
import { ConfigService } from './config.service'
import { Observable } from 'rxjs/Observable';

@Injectable()
export class OptionsService {
  constructor(private configService: ConfigService) {
  }

  conf: any;

  getOptions(): Observable<any> {
    return this.configService.getConfig('app/services/conf/options.service.json');
  }
}