import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class ConfigService {
  constructor(private http: Http) {
  }

  getConfig(file): Observable<any> {
    return this.http.get(file)
      .map((res: Response) => res.json());
  }
}