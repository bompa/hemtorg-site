import { Injectable, Inject, ElementRef } from '@angular/core';
import { Map } from '../interfaces/map.interface'
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import { ConfigService } from '../services/config.service'
import { MapConfig, MapConfigProperties, INJECT_MAP_CONFIG } from '../interfaces/mapconfig.interface'
import { SearchResult } from '../interfaces/searchresult.interface'
import { GeoLocation } from 'ht-public/interfaces/geolocation.interface'
import { RealEstateProperties } from 'ht-public/interfaces/realestateproperties.interface'
import { PROPERTY_TYPE } from '../interfaces/realestate.interface'
import * as factory from '../model/mapconfig.model'

declare var google: any;
declare var $: any

@Injectable()
export class MapService {
  private config: MapConfigProperties;

  private mapObservable: Observable<any>;

  private markers: any[] = [];

  constructor( @Inject(INJECT_MAP_CONFIG) private mapConfig: MapConfig) {
    this.mapObservable = new Observable(subscriber => {
      mapConfig.config.subscribe(conf => this.init(conf, subscriber));
    });
  }

  get map() {
    return this.mapObservable;
  }

  init(config: MapConfigProperties, subscriber) {
    this.config = config;
    let mapElementId = config.mapElementId ? config.mapElementId : 'map';

    let mapElement = document.getElementById(mapElementId);
    if (!mapElement) {
      console.debug('The map element \'map\' is not defined.');
      subscriber.next(null);
    }
    else {
      let map = new google.maps.Map(mapElement, {
        zoom: 7,
        center: { lat: this.config.location.lat, lng: this.config.location.lng }
      });
      subscriber.next(map);
    }
    subscriber.complete();
  }

  getIconUrl(realEstate: RealEstateProperties): string {
    var assetBase = 'assets/images/';
    switch (realEstate.type) {
      case PROPERTY_TYPE.APARTMENT.valueNumber:
        return assetBase + 'marker_apartment.png';
      case PROPERTY_TYPE.HOUSE.valueNumber:
        return assetBase + 'marker_house.png';
      case PROPERTY_TYPE.ROW_HOUSE.valueNumber:
        return assetBase + 'marker_rowhouse.png';
    }
    return assetBase + 'marker_apartment.png';
  }

  createMarkers(realEstates: RealEstateProperties[], onMarkerClick?: () => void) {
    // Can not create markers until map object is available
    this.mapObservable.subscribe(map => {

      // Clear out the old markers.
      this.markers.forEach(function (marker) {
        marker.setMap(null);
      });
      this.markers = [];

      // Create new markers
      for (let realEstate of realEstates) {

        if (!realEstate.loc) {
          console.info('Skipping marker without location');
          continue;
        }

        let place: any = {};
        if (realEstate.placeID) {
          place = {
            placeId: realEstate.placeID
          }
        }
        place.location = { lat: Number(realEstate.loc.lat), lng: Number(realEstate.loc.lng) };
        let iconUrl = this.getIconUrl(realEstate);
        let marker = new google.maps.Marker({
          map: map,
          icon: iconUrl,
          place: place,
          title: realEstate.address
        });

        let styleString = '';
        if (realEstate.images && (realEstate.images.length != 0)) {
          styleString = 'style="height: 250px; background-size:cover; background-image: url(' + realEstate.images[0] + ');"';
        }
        //data-toggle="modal" data-target=".bs-example-modal-lg onclick="myFunction()""
        let contentString = '<div id="content">' +
          '<div id="siteNotice">' +
          '</div>' +
          '<div id="headingContent"' + styleString + ' >' +
          '<h1 id="firstHeading" class="firstHeading" style="color: white; text-shadow: 2px 2px #5a5a5a; text-align: center;">' + realEstate.address + '</h1>' +
          '</div>' +
          '<div id="bodyContent">' +
          '<div style="padding-top: 5px;">' +
          '<button id="modal-marker-button-id" class="btn btn-default">View All Images</button>' +
          '</div>' +
          '<hr/><div>' +
          '<p>Full address </p>' +
          '<p>' + realEstate.address + '</p>' +
          '<p>' + realEstate.postcode + ' ' + realEstate.city + '</p>' +
          '</div>' +
          '<hr/><p>This property was registred by <b>' + realEstate.owner + '</b></p>' +
          '</div>' +
          '</div>';
        let infoWindow = new google.maps.InfoWindow({
          content: contentString
        });

        marker.addListener('click', function () {
          infoWindow.open(map, marker);
          if (onMarkerClick) {
            onMarkerClick();
          }
        });
        this.markers.push(marker);
      }
    });
  }

  createPlaceResult(place): SearchResult {
    if (!place) {
      console.error('Received invalid place object');
      return {};
    }

    let result: SearchResult = {};
    if (place.place_id) {
      result.placeID = place.place_id;
    }
    if (place.geometry) {
      let geoLoc: GeoLocation = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng() };
      result.location = geoLoc;
    }
    else {
      console.error('Place missing geometry.location information.')
    }

    let streetRoute = '';
    let streetNr = '';

    for (let i in place.address_components) {
      let component = place.address_components[i];
      for (let j in component.types) {  // Some types are ["country", "political"]
        if (component.types[j] === 'route') {
          streetRoute = component.long_name;
        }
        else if (component.types[j] === 'street_number') {
          streetNr = component.long_name;
        }
        else if (component.types[j] === 'postal_town' || component.types[j] === 'locality') {
          result.city = component.long_name;
        }
        else if (component.types[j] === 'postal_code') {
          result.postcode = component.long_name;
        }
      }
    }
    result.address = streetRoute + ' ' + streetNr;

    return result;
  }

  createSearchBox(searchBoxID, typesConfig, resultCallback?: (placeResult: SearchResult) => any) {

    this.mapObservable.subscribe(map => {
      let element = $('#' + searchBoxID);
      if (element) {

        let options = {
          types: typesConfig,
          componentRestrictions: { country: "se" }
        };

        var serviceInstance = this;
        let autocomplete = new google.maps.places.Autocomplete(element.get(0), options);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
          let place = this.getPlace();
          console.info('Places changed');

          let result = serviceInstance.createPlaceResult(place);
          if (resultCallback) {
            resultCallback(result);
          }
        });
        return true;
      }
    });
  }

  createMapSearchBox(searchBoxID) {
    this.mapObservable.subscribe(map => {
      if (map) {
        // Create the search box and link it to the UI element.
        var input = document.getElementById(searchBoxID);
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function (marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function (place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }
    })
  }

}
