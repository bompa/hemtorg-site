import { Injectable, ElementRef } from '@angular/core';
import { Map } from '../interfaces/map.interface'
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import { ConfigService } from '../services/config.service'
import { MapConfig, MapConfigProperties } from '../interfaces/mapconfig.interface'
import { GeoLocation } from 'ht-public/interfaces/geolocation.interface'
import { RealEstateProperties } from 'ht-public/interfaces/realestateproperties.interface'
import * as factory from '../model/mapconfig.model'

@Injectable()
export class MapOptionsFileService implements MapConfig {
  config: Observable<MapConfigProperties>;

  constructor(private configService: ConfigService) {

    this.config = new Observable(subscriber => {
      this.configService.getConfig('app/services/conf/map.service.json')
        .subscribe(confData => this.init(factory.createMapConfigProps(confData), subscriber));
    });
  }

  private init(config: MapConfigProperties, subscriber) {
    subscriber.next(config);
    subscriber.complete();
  }
}
