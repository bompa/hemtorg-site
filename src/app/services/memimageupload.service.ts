import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";

export interface Header {
  header: string;
  value: string;
}

@Injectable()
export class MemImageService {
  private imageList: any[] = [];
  private imageFileList: File[] = [];

  get images() {
    return this.imageList;
  }

  get files() {
    return this.imageFileList;
  }

  //public setUrl(url: string) {
  //}

  static IMG_MAX_WIDTH: number = 1024;

  public addImage(src: string, image: File, headers?: Header[]) {
    this.imageFileList.push(image);

    return Observable.create(observer => {
      this.downscaleImage(image, src, MemImageService.IMG_MAX_WIDTH, image.type, {},
        (result) => {
          this.imageList.push(result);
          observer.next({ status: "ok" });
          observer.complete();
        })
    });
  }

  private checkUrl() {
  }

  public downscaleImage(file, src, newWidth, imageType, imageArguments, callback) {
    "use strict";
    var image, oldWidth, oldHeight, newHeight, canvas, ctx, newDataUrl;

    // Provide default values
    imageType = imageType || "image/jpeg";
    imageArguments = imageArguments || 0.7;

    // Create a temporary image so that we can compute the height of the downscaled image.
    image = new Image();
    image.addEventListener("load", () => {
      oldWidth = image.width;
      oldHeight = image.height;
      if (oldWidth > newWidth) {
        newHeight = Math.floor(oldHeight / oldWidth * newWidth)

        // Create a temporary canvas to draw the downscaled image on.
        canvas = document.createElement("canvas");
        canvas.width = newWidth;
        canvas.height = newHeight;

        // Draw the downscaled image on the canvas and return the new data URL.
        ctx = canvas.getContext("2d");
        ctx.drawImage(image, 0, 0, newWidth, newHeight);
        callback(canvas.toDataURL(imageType));
      }
      else {
        callback(src);
      }
    });
    image.src = src;
  }
}