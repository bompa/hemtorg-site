import { Injectable } from '@angular/core';
import { RealEstateProperties } from 'ht-public/interfaces/realestateproperties.interface';
import { RealEstate } from '../interfaces/realestate.interface';
import { MapConfig } from '../interfaces/mapconfig.interface';
import { createEmptyRealEstate } from '../model/realestate.model';
import { RealEstateService } from '../services/realestate.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RegistrationService {
  private realEstate: RealEstate;
  private imageFiles: File[];
  private httpStatus;
  private isRegistered: boolean = false;

  constructor(private realEstateService: RealEstateService) {
    let stored = localStorage.getItem('reg_property');
    if (stored) {
      this.realEstate = JSON.parse(stored);
    }
  }

  get status() {
    return this.httpStatus;
  }

  get images() {
    return this.imageFiles;
  }

  get registrationData() {
    return this.realEstate;
  }

  get registered() {
    return this.isRegistered;
  }

  public hasRegistrationData(): boolean {
    return this.realEstate ? true : false;
  }

  public setRegistrationData(realEstate: RealEstate, imageFiles: File[]) {
    this.realEstate = realEstate;
    this.imageFiles = imageFiles;
    localStorage.setItem('reg_property', JSON.stringify(realEstate));
    return this;
  }

  public regPreviewMapConfig(): MapConfig {
    return {
      config: new Observable(subs => {
        subs.next({
          mapElementId: 'preview-map',
          location: this.realEstate.loc
        });
      })
    };
  }

  /**
   * Register using previosly assinged data using 'setRegistrationData'
   */
  register() {
    return new Observable(subscriber => {
      if (!this.realEstate) {
        subscriber.error('Invalid state, missing realEstate data');
      }
      else {
        this.realEstateService.create(this.realEstate)
          .catch(err => {
            subscriber.error(err);
            return Promise.reject(err);
          })
          .subscribe(status => {
            if (status == 200) {
              this.isRegistered = true;
              this.realEstate = null;
              this.imageFiles = null;
              localStorage.removeItem('reg_property');
              subscriber.next(true);
            }
            else {
              subscriber.next(false);
            }
            subscriber.complete();
          },
          error => {
            subscriber.error(error);
          });
      }
    });
  }

}