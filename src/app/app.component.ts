import { Component, ChangeDetectorRef, ViewChild, OnInit, ElementRef } from '@angular/core';
import { tokenNotExpired, JwtHelper } from 'angular2-jwt';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { TranslateService } from '@ngx-translate/core';
import { MediaChange, ObservableMedia } from "@angular/flex-layout";
import { MdInputDirective } from '@angular/material'
import { Subscription } from "rxjs/Subscription";
import { AuthGuard } from './auth.guard';
import { MapService } from './services/map.service'
import { SearchService } from './services/search.service'
import { SearchResult } from './interfaces/searchresult.interface'

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.md.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private title = 'HemTorg';
  private searchValue: string = '';
  private search: any;

  @ViewChild('searchInput') public searchInput: ElementRef;

  constructor(private router: Router,
    private ref: ChangeDetectorRef,
    private flexMedia: ObservableMedia,
    private authService: AuthenticationService,
    private langService: TranslateService,
    private route: ActivatedRoute,
    private authGuard: AuthGuard,
    private mapService: MapService,
    private searchService: SearchService) {

    this.updateCurrentUser();
  }

  user: { name: string, email: string, picture: string };

  ngOnInit() {
    this.mapService.createSearchBox('search-input-id', ['geocode'], (placeResult) => { this.searchResult(placeResult) });
    this.setInputFocusClasses("search-input-id");

    this.langService.setDefaultLang('se');
    $('#arrowBackID').hide();
  }

  searchResult(placeResult: SearchResult) {
    this.search = placeResult;
  }

  setInputFocusClasses(inputElemID) {
    var self = this;
    let arrowBack = $('#arrowBackID');
    arrowBack.click(() => {
      if (!this.searchInput.nativeElement.value) {
        var press = $.Event("keypress");
        press.ctrlKey = false;
        press.which = 9;
        $("#search-input-id").trigger(press);
        //this.ref.detectChanges();
      }
    });
    $('#' + inputElemID)
      .focusout(function () {
        if (!self.searchInput.nativeElement.value) {
          $("#search-bar-container-id").addClass("search-bar-focusout").removeClass("search-bar-focus").removeClass("search-bar-focus-xs");
          $(".toolbar-entry").show();
          arrowBack.hide();
        }
      })
      .focus(function () {
        if (!self.searchInput.nativeElement.value) {
          let focusClassName = "search-bar-focus";
          if (self.flexMedia.isActive('xs')) {
            $(".toolbar-entry").hide();
            focusClassName = "search-bar-focus-xs";
            arrowBack.show();
          }
          $("#search-bar-container-id").removeClass("search-bar-init").removeClass("search-bar-focusout").addClass(focusClassName);
        }
      });
  }

  closeSearch() {
    $("#search-bar-container-id").addClass("search-bar-focusout").removeClass("search-bar-focus").removeClass("search-bar-focus-xs");
    $(".toolbar-entry").show();
    let arrowBack = $('#arrowBackID');
    arrowBack.hide();

    if (this.searchInput.nativeElement.value) {
      this.searchInput.nativeElement.value = '';
      $('#arrowBackID').trigger('click');
    }
    this.ref.detectChanges();
  }

  onEnterSearch(value) {
    this.searchService.search(this.search);
  }

  onSearchKeyUp(value) {
    console.log('key');
  }

  login() {
    return this.authService.login(
      (status: boolean) => {
        if (status) {
          this.updateCurrentUser();
          this.ref.detectChanges();
        }
      });
  }

  logout() {
    let res = this.authService.logout();
    this.ref.detectChanges();
    if (!this.authGuard.canActivate(this.route.snapshot, this.router.routerState.snapshot)) {
      this.router.navigate(['/']);
    }
    return res;
  }

  loggedIn() {
    return this.authService.loggedIn();
  }

  isActive(profile) {
    return this.authService.loggedIn();
  }

  updateCurrentUser() {
    this.user = this.authService.getCurrentUser();
  }

  goToProfile() {
    this.router.navigate(['/profile']);
  }
}
