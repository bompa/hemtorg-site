import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';

import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { AccordionModule } from 'ng2-bootstrap/accordion';
import { ModalModule } from 'ng2-bootstrap/modal';
import { CommonModule, CurrencyPipe } from "@angular/common";
import { TabsModule } from 'ng2-bootstrap/tabs';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from "@angular/flex-layout";

import { AuthGuard } from './auth.guard';
import { RealEstateService } from './services/realestate.service';
import { AuthenticationService } from './services/authentication.service';
import { AppComponent } from './app.component';
import { ProfileComponent } from './components/profile.component';
import { MapComponent } from './components/map.component';
import { MapService } from './services/map.service';
import { MapConfig, MapConfigProperties, INJECT_MAP_CONFIG } from './interfaces/mapconfig.interface'
import { ConfigService } from './services/config.service';
import { MapOptionsFileService } from './services/mapoptionsfile.service';
import { RegisterPropertyComponent } from './components/regproperty.component';
import { ProgressComponent } from './components/progress.component';
import { PreviewComponent } from './components/preview.component';
import { RegPropertyDialog } from './components/regpropertydialog.component';
import { AboutComponent } from './components/about.component';
import { FaqComponent } from './components/faq.component';
import { OptionsService } from './services/options.service';
import { RegistrationService } from './services/registration.service';
import { MemImageService } from './services/memimageupload.service';
import { SearchService } from './services/search.service';
import { ImageUploadComponent } from './components/image-upload.component';
import { FileDropDirective } from './directives/file-drop.directive';
import { routing } from './app.routes';

export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    MapComponent,
    RegisterPropertyComponent,
    ProgressComponent,
    PreviewComponent,
    RegPropertyDialog,
    AboutComponent,
    ImageUploadComponent,
    FileDropDirective,
    FaqComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AccordionModule.forRoot(),
    ModalModule.forRoot(),
    CommonModule,
    HttpModule,
    TabsModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    }),
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    routing
  ],
  providers: [
    AuthGuard,
    RealEstateService,
    AuthenticationService,
    MapService,
    { provide: INJECT_MAP_CONFIG, useClass: MapOptionsFileService },
    ConfigService,
    OptionsService,
    MemImageService,
    RegistrationService,
    MapOptionsFileService,
    SearchService,
    CurrencyPipe
  ],
  entryComponents: [RegPropertyDialog],
  bootstrap: [AppComponent]
})
export class AppModule {
}