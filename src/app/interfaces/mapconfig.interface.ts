import { Injectable, InjectionToken } from '@angular/core';
import { Observable } from 'rxjs/Observable';

export interface MapConfigProperties {
  mapElementId: string,
  location: {
    lat: number;
    lng: number;
  }
};

export interface MapConfig {
  config: Observable<MapConfigProperties>
};


export let INJECT_MAP_CONFIG = new InjectionToken<MapConfig>('MapConfig');

