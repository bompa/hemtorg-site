import { ImageData } from 'ht-public/interfaces/imagedata.interface'

export interface ImageAdder {  
  addImage(image: ImageData): void;
};
