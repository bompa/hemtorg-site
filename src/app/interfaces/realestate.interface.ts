import { ImageAdder } from './imageadder.interface'
import { RealEstateProperties } from 'ht-public/interfaces/realestateproperties.interface'

export interface RealEstate extends RealEstateProperties, ImageAdder { }

export var PROPERTY_TYPE = {
  APARTMENT: { value: "0", valueNumber: 0, name: "Apartment" },
  HOUSE: { value: "1", valueNumber: 1, name: "House" },
  ROW_HOUSE: { value: "2", valueNumber: 2, name: "Row House" },
}