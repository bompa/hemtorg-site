import { GeoLocation } from 'ht-public/interfaces/geolocation.interface'

export interface SearchResult {
  address?: string;
  postcode?: string;
  city?: string;
  placeID?: string;
  location?: GeoLocation
}