import { RealEstateProperties } from 'ht-public/interfaces/realestateproperties.interface'
import { ImageAdder } from '../interfaces/imageadder.interface'
import { GeoLocation } from 'ht-public/interfaces/geolocation.interface'
import { PriceRange } from 'ht-public/interfaces/pricerange.interface'
import { ImageData } from 'ht-public/interfaces/imagedata.interface'
import { RealEstate, PROPERTY_TYPE } from '../interfaces/realestate.interface'

export function createImageAdder(imageContainer): ImageAdder {
  return {
    addImage: (image: ImageData): void => {
      if (image) {
        if (!imageContainer.images) {
          imageContainer.images = [];
        }
        imageContainer.images.push(image);
      }
    }
  }
}

export function createRealEstate(realEstate: RealEstateProperties): RealEstate {
  let ret = {
    address: realEstate.address,
    postcode: realEstate.postcode,
    type: realEstate.type,
    size: realEstate.size,
    priceRange: realEstate.priceRange,
    monthlyFee: realEstate.monthlyFee,
    city: realEstate.city,
    owner: realEstate.owner,
    placeID: realEstate.placeID,
    loc: realEstate.loc,
    images: realEstate.images,
    isPrivate: realEstate.isPrivate,
    addImage: undefined,
  }
  ret.addImage = createImageAdder(ret).addImage
  return ret;
}

export function createEmptyRealEstate(): RealEstate {
  return createRealEstate({
    address: "",
    postcode: "",
    type: -1,
    size: "",
    priceRange: { min: -1, max: -1 },
    monthlyFee: -1,
    city: "",
    owner: "",
    placeID: "",
    loc: { lng: 0, lat: 0 },
    isPrivate: true
  });
}

export function realEstateTypeName(numberValue: any): string {
  let res: number = -1;
  if ("string" === typeof numberValue) {
    res = parseInt(numberValue, 10);
  }
  else if ("number" === typeof numberValue) {
    res = numberValue
  }

  if (res !== -1) {
    for (let k in PROPERTY_TYPE) {
      if (PROPERTY_TYPE.hasOwnProperty(k) && PROPERTY_TYPE[k].valueNumber == res) {
        return PROPERTY_TYPE[k].name;
      }
    }
  }

  return "";
}