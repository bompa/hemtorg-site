import { MapConfigProperties } from '../interfaces/mapconfig.interface'

export function createMapConfigProps(confData: any): MapConfigProperties {
return { mapElementId: confData.mapElementId, location: { lat: Number(confData.loc.lat), lng: Number(confData.loc.lng) } };
}  