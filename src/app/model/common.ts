export function hasAnyProperties(obj: any): boolean {
  for (var prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      return true;
    }
  }
  return false;
}