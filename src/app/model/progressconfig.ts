export class ProgressConfig {
  color: string= 'primary';
  mode:string = 'determinate';
  value: number = 0;
}