import { Component } from '@angular/core';
import { RealEstateService } from '../services/realestate.service';
import { RealEstateProperties } from 'ht-public/interfaces/realestateproperties.interface';
import { AuthenticationService } from '../services/authentication.service';
import { Query } from 'ht-public/queries/query'
import { RealEstatesQuery } from 'ht-public/queries/realestatesquery'

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {

  constructor(private realEstateService: RealEstateService,
    private authService: AuthenticationService) {

    this.updateCurrentUser();
    this.updateRealEstates();
  }

  user: { name: string, email: string, picture: string };
  realEstates: RealEstateProperties[];

  updateCurrentUser() {
    this.user = this.authService.getCurrentUser();
  }

  updateRealEstates() {
    let q: Query = RealEstatesQuery.create();
    q.filter.userID = Query.CURRENT_USER_VALUE;
    this.realEstateService.list(q)
      .subscribe(realEstates => {
        this.realEstates = realEstates;
      });
  }

  getProfileUser() {

  }
}