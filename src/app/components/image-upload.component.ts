import { Component, EventEmitter, ChangeDetectorRef, Input, OnInit, Output } from '@angular/core';
import { Header, MemImageService } from '../services/memimageupload.service';

export class FileHolder {
  public serverResponse: { status: number, response: any };
  public pending: boolean = false;

  constructor(public src: string, public file: File) {
  }
}

@Component({
  selector: 'image-upload',
  templateUrl: 'image-upload.component.md.html',
  styleUrls: ['image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {
  @Input() max: number = 100;
  @Input() url: string;
  @Input() headers: Header[];
  @Input() preview: boolean = true;
  @Input() maxFileSize: number;

  @Output()
  isPending: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output()
  onFileUploadFinish: EventEmitter<FileHolder> = new EventEmitter<FileHolder>();
  @Output()
  onRemove: EventEmitter<FileHolder> = new EventEmitter<FileHolder>();

  files: FileHolder[] = [];
  showFileTooLargeMessage: boolean = false;

  private fileCounter: number = 0;
  private pendingFilesCounter: number = 0;

  isFileOver: boolean = false;

  @Input()
  buttonCaption: string = "Select Images";
  @Input()
  dropBoxMessage: string = "Drop your images here!";
  @Input()
  fileTooLargeMessage: string;

  constructor(private imageService: MemImageService,
    private changes: ChangeDetectorRef) {
  }

  ngOnInit() {
    if (!this.fileTooLargeMessage) {
      this.fileTooLargeMessage = 'An image was too large and was not uploaded.' + (this.maxFileSize ? (' The maximum file size is ' + this.maxFileSize / 1024) + 'KiB.' : '');
    }
  }

  fileChange(fileList: FileList) {
    let files: File[] = [];
    for (let i = 0; i < fileList.length; i++) {
      files.push(fileList[i]);
    }
    this.fileChangeArray(files);
  }

  fileChangeArray(files: File[]) {
    let remainingSlots = this.countRemainingSlots();
    let filesToUploadNum = files.length > remainingSlots ? remainingSlots : files.length;

    if (this.url && filesToUploadNum != 0) {
      this.isPending.emit(true);
    }

    this.fileCounter += filesToUploadNum;
    this.showFileTooLargeMessage = false;
    this.uploadFiles(files, filesToUploadNum);
  }

  deleteFile(file: FileHolder): void {
    let index = this.files.indexOf(file);
    this.files.splice(index, 1);
    this.fileCounter--;

    this.onRemove.emit(file);
    this.changes.detectChanges();
  }

  fileOver(isOver) {
    this.isFileOver = isOver;
  }

  private uploadFiles(files: File[], filesToUploadNum: number) {
    for (let i = 0; i < filesToUploadNum; i++) {
      let file = files[i];

      if (this.maxFileSize && file.size > this.maxFileSize) {
        this.showFileTooLargeMessage = true;
        continue;
      }

      let img = document.createElement('img');
      img.src = window.URL.createObjectURL(file);

      let reader = new FileReader();
      reader.addEventListener('load', (event: any) => {
        let fileHolder: FileHolder = new FileHolder(event.target.result, file);

        this.uploadSingleFile(fileHolder);

        this.files.push(fileHolder);

      }, false);


      reader.readAsDataURL(file);
    }
  }

  private onResponse(response, fileHolder: FileHolder) {
    fileHolder.serverResponse = response;
    fileHolder.pending = false;
    this.changes.detectChanges();

    this.onFileUploadFinish.emit(fileHolder);

    if (--this.pendingFilesCounter == 0) {
      this.isPending.emit(false);
    }
  }

  private uploadSingleFile(fileHolder: FileHolder) {
    if (this.url) {
      this.pendingFilesCounter++;
      fileHolder.pending = true;

      this.imageService
        .addImage(fileHolder.src, fileHolder.file, this.headers)
        .subscribe(
        response => this.onResponse(response, fileHolder),
        error => {
          this.onResponse(error, fileHolder);
          this.deleteFile(fileHolder);
        }
        );
    } else {
      this.onFileUploadFinish.emit(fileHolder);
    }
  }

  private countRemainingSlots() {
    return this.max - this.fileCounter;
  }
}
