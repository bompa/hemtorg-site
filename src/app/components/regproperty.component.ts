import { Component, Inject, ElementRef, OnInit, OnDestroy, EventEmitter, Output, ViewChild } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { MdInputDirective, MdInputContainer } from '@angular/material';
import { MdSlideToggle, MdSelect, MdOption } from '@angular/material';

import { MemImageService } from '../services/memimageupload.service';
import { RegistrationService } from '../services/registration.service';
import { RealEstateService } from '../services/realestate.service';
import { RealEstateProperties } from 'ht-public/interfaces/realestateproperties.interface';
import { RealEstate, PROPERTY_TYPE } from '../interfaces/realestate.interface'
import { GeoLocation } from 'ht-public/interfaces/geolocation.interface';
import { PriceRange } from 'ht-public/interfaces/pricerange.interface';
import * as factory from '../model/realestate.model'
import { MapService } from '../services/map.service'
import { AuthenticationService } from '../services/authentication.service';
import { MapOptionsFileService } from '../services/mapoptionsfile.service';
import { TranslateService } from '@ngx-translate/core';
import { RegPropertyDialog } from './regpropertydialog.component';
import { hasAnyProperties } from '../model/common';
import { ImageUploadComponent } from './image-upload.component';
import { ProgressConfig } from '../model/progressconfig';


/**
 * Component for registration of property
 */
@Component({
  selector: 'registerproperty',
  templateUrl: './regproperty.component.md.html',
  styleUrls: ['./regproperty.component.css']
})
export class RegisterPropertyComponent implements OnInit, OnDestroy {

  private realEstate: RealEstate = factory.createEmptyRealEstate();
  private ownerEmail: string;
  private registerPropertyForm: FormGroup;
  private placeID: string;
  private loc: GeoLocation;
  private typeValue: string;
  private price: { min: string, max: string } = { min: '', max: '' };
  private monthlyFee: string;
  private showMonthlyFee: boolean = false;
  private lotArea: string;
  private showLotArea: boolean = false;
  private saveInterval: Subscription;
  private changed: boolean = true;
  private type = PROPERTY_TYPE;

  private submitted = false;

  public disabled: boolean = true;
  public regStatusText: string = '';

  private selectImagesTxt: string = 'Select Images...';
  private dropImagesTxt: string = '...or Drop your Images Here';

  @Output()
  onNext: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('postalcodeInput') public postalcodeInput: ElementRef;
  @ViewChild('cityInput') public cityInput: ElementRef;
  @ViewChild('propertyTypeSelect') public selectedType: MdSelect;
  @ViewChild('imageUploadComponent') public imageUpload: ImageUploadComponent;

  constructor(private fb: FormBuilder,
    private router: Router,
    private realEstateService: RealEstateService,
    private mapService: MapService,
    private authService: AuthenticationService,
    private imageService: MemImageService,
    private langService: TranslateService,
    private currency: CurrencyPipe,
    private registrationService: RegistrationService) { }

  setUserValuesIfLoggedIn() {
    if (this.authService.loggedIn()) {
      let user = this.authService.getCurrentUser();
      this.registerPropertyForm.patchValue({
        emailctrl: user.email,
        namectrl: user.name
      });
    }
  }

  ngOnInit(): void {
    this.buildForm();
    this.setUserValuesIfLoggedIn();

    this.mapService.createSearchBox('address-input',
      ['geocode'],
      (placeResult) => {
        // Update form data
        this.registerPropertyForm.patchValue({
          addressctrl: placeResult.address,
          cityctrl: placeResult.city,
          postcodectrl: placeResult.postcode
        });
        // Run click element to run MD set value animation, otherwise placeholder value will
        // be on top of value.
        this.postalcodeInput.nativeElement.click();
        this.cityInput.nativeElement.click();

        // Update non-form data
        this.loc = placeResult.location;
        this.placeID = placeResult.placeID;
      });

    this.mapService.createSearchBox('city-input',
      ['(cities)'],
      (placeResult) => {
        this.registerPropertyForm.patchValue({
          cityctrl: placeResult.city
        });
      });

    this.langService.get(this.selectImagesTxt).subscribe((res: string) => {
      this.selectImagesTxt = res;
    });
    this.langService.get(this.dropImagesTxt).subscribe((res: string) => {
      this.dropImagesTxt = res;
    });
  }

  clearIfNegative(val: string): string {
    if (val == '-1') {
      return '';
    }
    return val;
  }

  markCtrlsDirty(names: string[]) {
    for (let name of names) {
      this.registerPropertyForm.get(name).markAsDirty();
    }
  }

  ngAfterViewInit() {
    if (this.registrationService.hasRegistrationData()
      && this.registrationService.images) {
      this.imageUpload.fileChangeArray(this.registrationService.images);
    }

    if (this.registrationService.hasRegistrationData()) {
      let data = this.registrationService.registrationData

      this.price.min = this.clearIfNegative(data.priceRange.min.toString());
      this.price.max = this.clearIfNegative(data.priceRange.max.toString());
      this.monthlyFee = this.clearIfNegative(data.monthlyFee.toString());

      this.markCtrlsDirty(['pricectrlmin', 'pricectrlmax', 'pricectrlmonthly'])
      this.registerPropertyForm.patchValue({
        addressctrl: data.address,
        postcodectrl: data.postcode,
        proptypectrl: data.type,
        sizectrl: data.size,
        pricectrlmin: this.price.min,
        pricectrlmax: this.price.max,
        pricectrlmonthly: this.monthlyFee,
        cityctrl: data.city
      });
      //this.selectedType.writeValue("0");

      this.placeID = data.placeID;
      this.loc = data.loc;
    }

    this.saveInterval = Observable.interval(RegisterPropertyComponent.SAVE_INTERVALL).subscribe(x => {
      if (this.changed) {
        this.save();
        this.changed = false;
      }
    });
  }

  private static SAVE_INTERVALL = 10000;

  ngOnDestroy() {
    this.save();
  }

  save() {
    this.registrationService.setRegistrationData(
      this.createRegistrationRealEstate(),
      this.imageService.files);
  }

  private getAddedFiles() {
    return this.imageService.images;
  }

  buildForm(): void {
    this.registerPropertyForm = this.fb.group({
      namectrl: [this.realEstate.owner, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(50)
      ]],
      emailctrl: [this.ownerEmail, [Validators.required, CustomValidators.email]],
      cityctrl: [this.realEstate.city, Validators.required],
      addressctrl: [this.realEstate.address, Validators.required],
      postcodectrl: [this.realEstate.postcode,
      [Validators.required, Validators.pattern('[0-9]{5}$|([0-9]{3} [0-9]{2}$)')]],
      proptypectrl: [this.realEstate.type, Validators.required],
      sizectrl: [this.realEstate.size],
      pricectrlmin: [this.price.min],
      pricectrlmax: [this.price.max],
      pricectrlmonthly: [this.monthlyFee],
      lotsizectrl: [this.lotArea]
    });

    this.registerPropertyForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  private createRegistrationRealEstate() {
    let realEstate = factory.createRealEstate(
      {
        address: this.registerPropertyForm.value.addressctrl,
        postcode: this.registerPropertyForm.value.postcodectrl,
        type: this.getMdOptionSelectedValueNumber(this.selectedType.selected),
        size: this.registerPropertyForm.value.sizectrl,
        priceRange: this.realEstate.priceRange,
        monthlyFee: this.realEstate.monthlyFee,
        city: this.registerPropertyForm.value.cityctrl,
        owner: this.registerPropertyForm.value.namectrl,
        placeID: this.placeID,
        loc: this.loc,
        isPrivate: true
      }
    );

    for (let image of this.imageService.images) {
      if (image) {
        realEstate.addImage(image);
      }
    }
    return realEstate;
  }

  public previewProperty() {
    this.realEstate = this.createRegistrationRealEstate();
    this.registrationService.setRegistrationData(this.realEstate, this.imageService.files)
    this.router.navigate(['/preview']);
  }

  transformCurrencyInput(rawInput, out?: { out: number }): string {
    var input = rawInput.replace(/[\D\s\._\-]+/g, "");
    input = input ? parseInt(input, 10) : 0;
    if (out) {
      out.out = input;
    }
    return (input === 0) ? "" : this.currency.transform(input, 'SEK', true, '1.0-0');
  }

  getFormatedControllerValue(form: FormGroup, patchValues, ctrlName, parsed: { out: number }) {
    parsed.out = 0;
    let ctrl = form.get(ctrlName);
    if (ctrl && ctrl.dirty) {
      patchValues[ctrlName] = this.transformCurrencyInput(form.value[ctrlName], parsed);
      return true;
    }
    return false;
  }

  getMdOptionSelectedValue(selected: MdOption | MdOption[]): any {
    if (selected) {
      if ((<MdOption>selected).value) {
        return (<MdOption>selected).value;
      }
      else if ((<MdOption[]>selected).length) {
        return (<MdOption[]>selected)[0];
      }

    }
    return "";
  }

  getMdOptionSelectedValueNumber(selected: MdOption | MdOption[]): number {
    if (selected) {
      let value = this.getMdOptionSelectedValue(selected);
      let type = parseInt((<MdOption>selected).value);
      if (isNaN(type)) {
        return -1;
      }
      return type;
    }
    return -1;
  }

  checkCurrencyFormValues(form: FormGroup) {
    let patchValues: any = {};
    let parsed: { out: number } = { out: 0 };

    if (this.getFormatedControllerValue(form, patchValues, 'pricectrlmin', parsed)) {
      this.realEstate.priceRange.min = parsed.out;
    }
    if (this.getFormatedControllerValue(form, patchValues, 'pricectrlmax', parsed)) {
      this.realEstate.priceRange.max = parsed.out;
    }
    if (this.getFormatedControllerValue(form, patchValues, 'pricectrlmonthly', parsed)) {
      this.realEstate.monthlyFee = parsed.out;
    }

    if (hasAnyProperties(patchValues)) {
      form.patchValue(patchValues, { emitEvent: false });
    }
  }

  checkTypeFormValues(form) {
    let typeCtrl = form.get('proptypectrl');
    if (typeCtrl && typeCtrl.dirty) {
      this.showMonthlyFee = false;
      this.showLotArea = false;

      let selected = this.getMdOptionSelectedValue(this.selectedType.selected);
      if (selected === this.type.APARTMENT.value) {
        this.showMonthlyFee = true;
      }
      else if (selected === this.type.HOUSE.value) {
        this.showLotArea = true;
      }
    }
  }

  checkFormErrors(form) {
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onValueChanged(data?: any) {
    this.changed = true;
    if (!this.registerPropertyForm) {
      return;
    }

    const form = this.registerPropertyForm;
    this.checkCurrencyFormValues(form);
    this.checkTypeFormValues(form);
    this.checkFormErrors(form);
  }

  formErrors = {
    namectrl: '',
    emailctrl: '',
    addressctrl: '',
    postcodectrl: '',
    cityctrl: '',
    proptypectrl: ''
  };

  validationMessages = {
    'namectrl': {
      'required': 'Name is required.',
      'minlength': 'Name must be at least 1 character long.',
      'maxlength': 'Name cannot be more than 50 characters long.'
    },
    'emailctrl': {
      'required': 'Email is required.',
      'email': 'Invalid email format'
    },
    'addressctrl': {
      'required': 'Address is required.'
    },
    'postcodectrl': {
      'required': 'Post code is required.',
      'pattern': 'Must match patten nnn nn, e.g 123 45'
    },
    'cityctrl': {
      'required': 'City is required.'
    },
    'proptypectrl': {
      'required': 'Type is required.'
    }
  };
}
