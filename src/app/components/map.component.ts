import { Component, Renderer, OnInit, OnChanges, AfterContentInit, SimpleChanges, ViewChild } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap/modal';

import { MapService } from '../services/map.service'
import { RealEstateService } from '../services/realestate.service'
import { RealEstateProperties } from 'ht-public/interfaces/realestateproperties.interface'
import { MapConfig } from '../interfaces/mapconfig.interface'
import { MapOptionsFileService } from '../services/mapoptionsfile.service'

declare var $: any

@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, OnChanges {

  constructor(private mapService: MapService,
    private realEstateService: RealEstateService,
    private renderer: Renderer) { }

  private realEstates: RealEstateProperties[] = [];

  @ViewChild('markerCarouselModal') public markerCarouselModal: ModalDirective;

  ngOnInit() {
    this.mapService.map.subscribe(); // Make sure map is initalized
    this.updateMapMarkers();
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngAfterContentInit() {
  }

  updateMapMarkers() {
    this.realEstateService.list().subscribe(result => {
      // TODO: implement caching logic
      if (result && (result.length != this.realEstates.length)) {
        this.realEstates = result;
        this.mapService.createMarkers(this.realEstates,
          () => {
            let modalbtn = $("#modal-marker-button-id");
            if (modalbtn) {
              let simple = this.renderer.listen(modalbtn.get(0), 'click', (evt) => {
                this.markerCarouselModal.show();
              });
            }
          });
      }
    });

  }

  viewMarkerImages() {
    this.markerCarouselModal.show();
  }
}

