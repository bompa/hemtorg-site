import { Component, Input, OnInit } from '@angular/core';
import { MdTabsModule, MdTab, MdTabLabel } from '@angular/material';

import { ProgressConfig } from '../model/progressconfig';

/**
 * Component for registration of property
 */
@Component({
  selector: 'register-progress',
  templateUrl: './progress.component.md.html',
  styleUrls: ['./progress.component.css']
})
export class ProgressComponent implements OnInit {

  private progress: ProgressConfig = new ProgressConfig();

  public static GlobalStates = { info: 1, preview: 2, send: 3 };
  private states = ProgressComponent.GlobalStates;

  @Input()
  private state = 'info';

  static PROGRESS_VAL_STEP = 37;

  constructor() {
  }

  ngOnInit() {
    this.progress.value = ProgressComponent.PROGRESS_VAL_STEP * this.states[this.state];
  }
}
