import { Component, Inject, ViewChild } from '@angular/core';
import { MdDialog, MdDialogRef, MdDialogConfig, MD_DIALOG_DATA, MdProgressSpinner } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';

/**
 * Dialog component
 */
@Component({
  selector: 'dialog-result-dialog',
  templateUrl: './regpropertydialog.component.md.html',
})
export class RegPropertyDialog {
  private regOkTxt: string = "Sucessfuly registred property";
  private regProgressTxt: string = "Registering property...";
  private failRegTtx: string = "Failed to register property, server returned error ";
  private updateStateFunc: (done: boolean) => void;

  private text: string = '';
  private label: string = 'Property Registration';
  private labelTxt: string = '';
  private working: boolean = true;
  @ViewChild('regProgressSpinner') private progressSpinner: MdProgressSpinner;

  constructor(public dialogRef: MdDialogRef<RegPropertyDialog>,
    @Inject(MD_DIALOG_DATA) public data: any,
    private langService: TranslateService,
    private router: Router) {

    this.langService.get(this.label).subscribe((res: string) => {
      this.label = res;
      this.labelTxt = this.label + '...';
    });
    this.langService.get(this.regOkTxt).subscribe((res: string) => {
      this.regOkTxt = res;
    });
    this.langService.get(this.regProgressTxt).subscribe((res: string) => {
      this.regProgressTxt = res;
    });
    this.langService.get(this.failRegTtx).subscribe((res: string) => {
      this.failRegTtx = res;
    });
  }

  closeOk() {
    this.dialogRef.close('Ok');
    this.router.navigate(['/']);
  }

  state(ok: boolean, errorStatus?) {
    this.labelTxt = this.label;
    this.working = false;
    if (ok) {
      this.text = this.regOkTxt;
    }
    else {
      this.text = this.failRegTtx + (errorStatus || ' unknown-error');
    }
  }
}