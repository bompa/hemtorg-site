import { Component, OnInit, EventEmitter, Output, Renderer } from '@angular/core';
import { Router } from '@angular/router';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';

import { INJECT_MAP_CONFIG } from '../interfaces/mapconfig.interface'
import { RealEstateProperties } from 'ht-public/interfaces/realestateproperties.interface';
import { PROPERTY_TYPE } from '../interfaces/realestate.interface';
import { RegistrationService } from '../services/registration.service';
import { MapService } from '../services/map.service';
import { MapOptionsFileService } from '../services/mapoptionsfile.service';
import { realEstateTypeName } from '../model/realestate.model';
import { RegPropertyDialog } from './regpropertydialog.component';

declare var $: any

let mapServiceFactory = (regService: RegistrationService) => {
  return new MapService(regService.regPreviewMapConfig());
};

/**
 * Component for registration of property
 */
@Component({
  selector: 'preview',
  templateUrl: './preview.component.md.html',
  styleUrls: ['./preview.component.css'],
  providers: [
    {
      provide: MapService,
      useFactory: mapServiceFactory,
      deps: [RegistrationService]
    }
  ]
})
export class PreviewComponent implements OnInit {

  private property: RealEstateProperties;
  private dialogRef: MdDialogRef<RegPropertyDialog>;
  
  //@Output()
  //onBack: EventEmitter<any> = new EventEmitter<any>();
  //onRegister: EventEmitter<any> = new EventEmitter<any>();

  constructor(private registrationService: RegistrationService,
    private router: Router,
    private mapService: MapService,
    private dialog: MdDialog,
    private renderer: Renderer) {

    this.property = registrationService.registrationData;
  }


  ngOnInit() {
    this.mapService.map.subscribe(); // Make sure map is initalized
    this.createMarker();
  }

  getRealEstateTypeName(number) {
    return realEstateTypeName(number);
  }

  createMarker() {
    if (!this.registrationService.hasRegistrationData()) {
      console.error('Missing registration data for preview.')
      return;
    }

    this.mapService.createMarkers([this.property]);
  }

  public showChildModal(): void {
    let config: MdDialogConfig = { data: {} };
    this.dialogRef = this.dialog.open(RegPropertyDialog, config);
  }

  public hideChildModal(): void {
    this.dialog.closeAll();
  }

  public back() {
    this.router.navigate(['/register']);
  }

  public saveProperty() {
    this.register(true);
  }

  public registerProperty() {
    this.register(false);
  }

  private register(isPrivate: boolean) {
    this.showChildModal();
    this.registrationService.registrationData.isPrivate = isPrivate;
    this.registrationService.register()
      .subscribe(status => {
        if (status) {
          if (this.dialogRef) {
            this.dialogRef.componentInstance.state(true);
          }
        }
        else if (this.dialogRef) {
          this.dialogRef.componentInstance.state(false);
        }
      },
      error => {
        if (this.dialogRef) {
          this.dialogRef.componentInstance.state(false, error);
        }
      });
  }

} 
