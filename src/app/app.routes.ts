import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MapComponent } from './components/map.component';
import { ProfileComponent } from './components/profile.component';
import { RegisterPropertyComponent } from './components/regproperty.component';
import { PreviewComponent } from './components/preview.component';
import { AboutComponent } from './components/about.component';
import { FaqComponent } from './components/faq.component';
import { AuthGuard } from './auth.guard';

// Route Configuration
export const routes: Routes = [
  { path: '', component: MapComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'register', component: RegisterPropertyComponent },
  { path: 'preview', component: PreviewComponent },
  { path: 'about', component: AboutComponent },
  { path: 'faq', component: FaqComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);