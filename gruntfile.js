module.exports = function (grunt) {
  "use strict";

  grunt.initConfig({
    copy: {
      build: {
        files: [
          {
            expand: true,
            cwd: "./assets",
            src: ["**"],
            dest: "./dist/assets"
          },
          {
            expand: true,
            cwd: "./conf",
            src: ["**"],
            dest: "./dist/conf"
          }
        ]
      }
    },
    ts: {
      app: {
        tsconfig: true,
        files: [{
          src: ["src/\*\*/\*.ts", "!src/.baseDir.ts"],
          dest: "./dist"
        }],
        options: {
          module: "commonjs",
          target: "es6",
          sourceMap: true          
        }
      }
    },
    watch: {
      ts: {
        files: ["src/\*\*/\*.ts"],
        tasks: ["exec"]
      }
    },
    concurrent: {
      dev: {
        tasks: ['nodemon', 'watch'],
        options: {
          logConcurrentOutput: true
        }
      }
    },
    exec: {
      exec_build: 'ng build --dev --output-path=./dist-dev'
    },
    nodemon: {
      dev: {
        script: 'server.js',
        ignore: ['node_modules/**'],
        ext: 'js',
        watch: ['server'],
      }
    }
  });

  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks('grunt-exec');
  grunt.loadNpmTasks("grunt-concurrent");

  grunt.registerTask('build', ['exec']);
  grunt.registerTask('default', ['copy', 'concurrent']);
};