import { HemtorgSitePage } from './app.po';

describe('hemtorg-site App', function() {
  let page: HemtorgSitePage;

  beforeEach(() => {
    page = new HemtorgSitePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
